package itis.parsing;

interface ParkParsingService {

    Class<Park> parseParkData(String parkDatafilePath) throws ParkParsingException;

}
