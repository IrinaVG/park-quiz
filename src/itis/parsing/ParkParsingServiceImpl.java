package itis.parsing;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class ParkParsingServiceImpl implements ParkParsingService {

    private String str;
    private Class<?> park;
    private Object p;


    {
        try {
            park = Class.forName("itis.parsing.Park");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    Constructor constructor;

    {
        try {
            constructor = park.getConstructor(String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    Object parrk;

    {
        try {
            parrk = constructor.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    Field[] fields = parrk.getClass().getFields();

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Class<Park> parseParkData(String parkDatafilePath) throws ParkParsingException {

        String legalName = null;
        String foundationYear = null;
        String ownerOrganizationInn = null;
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(parkDatafilePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        try {
            str = bufferedReader.readLine();
            str = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (str !=null ){
            String[] name = str.split(":");

            if (name[0].equals("\"legalName\"")){
                legalName = name[1];
                legalName.replaceAll("\"\"", "");
            }
            if (name[0].equals("\"ownerOrganizationInn\"")){
                ownerOrganizationInn = name[1];
                ownerOrganizationInn.replaceAll("\"\"", "");
            }
            if (name[0].equals("\"foundationYear\"")){
                foundationYear = name[1];
                foundationYear.replaceAll("\"\"", "");
            }
            try {
                str = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Post postPath = new Post(posts[2],posts[3], Long.valueOf(posts[1]), authorArrayList.get(id-1));
            //System.out.println(postPath);
            //postArrayList.add(postPath);
            //post = bufferedReader1.readLine();
        }

        try {
            fields[0].setAccessible(true);
            fields[0].set(parrk, legalName);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            fields[1].setAccessible(true);
            fields[1].set(parrk, legalName);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        try {
            fields[2].setAccessible(true);
            fields[2].set(parrk, legalName);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        //write your code here
        return (Class<Park>) parrk;
    }
}
